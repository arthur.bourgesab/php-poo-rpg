<?php

class Hero {
    private $life;
    private $name;

    public function __construct($name) {
        $this->name = $name;
        $this->life = 100;
    }
    public function talk() {
        echo "Hi, I am a Hero";
    }
}